# Best4u Backend module for Magento 2

Best4u Backend module for Magento 2

## Installation
Installation via composer:

```
composer config repositories.best4u-backend vcs [bitbucket-repository-url]
```
```
composer require best4u/module-backend --no-update
```
```
composer update best4u/module-backend
```
```
bin/magento module:enable Best4u_Backend
```