<?php
/**
 * Copyright © Best4u Media B.V. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Best4u_Backend', __DIR__);
